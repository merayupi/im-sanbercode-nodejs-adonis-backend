
const sapa = (nama) => {
    return `halo selamat pagi, ${nama}`
}

const biodata = (nama, domisili, umur) => {
    return {
        nama,
        domisili,
        umur : Number(umur)
    }
}   

const check = (name, kelas, score) => {
    return {
        name,
        kelas,
        score : Number(score)
    }
}

const data = [
    { name: "Ahmad", kelas: "adonis"},
    { name: "Regi", kelas: "laravel"},
    { name: "Bondra", kelas: "adonis"},
    { name: "Iqbal", kelas: "vuejs" },
    { name: "Putri", kelas: "Laravel" }
  ]

const filterData = (kelas) => {
    for(let i = 0; i<data.length ;i++){
        return data.filter((el) => el['kelas'].toLowerCase().includes(kelas.toLowerCase()));
    }
    
}

exports.sapa = sapa
exports.biodata = biodata
exports.check = check
exports.filterData = filterData

