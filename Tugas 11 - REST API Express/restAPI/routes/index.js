var express = require('express');
var router = express.Router();

const employeeController = require('../controllers/employeeController')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register',employeeController.register)
router.get('/karyawan', employeeController.findAll)
router.post('/login', employeeController.login)

module.exports = router;
