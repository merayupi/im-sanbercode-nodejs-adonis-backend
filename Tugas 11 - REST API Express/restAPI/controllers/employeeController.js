const fs = require('fs')
const fsPromise = require('fs/promises')
const path = 'data.json'

class employeeController {
    static register(req,res){
        // res.status(200).json({message:'testing'})
        fs.readFile(path, (err,data) => {
            if(err){
                res.status(400).json({
                    error : 'data tidak terbaca'
                })
            }else{
                let existingData = JSON.parse(data)

                let {users} = existingData

                let { name, role, password} = req.body
                let newEmployee = {name, role, password, isLogin:false}
                
                users.push(newEmployee)
                let newData = {...existingData,users}

                fs.writeFile(path,JSON.stringify(newData), (err) => {
                    if(err){
                        res.status(400).json({error:'eror menyimpan data'})
                    }else{
                        res.status(201).json({
                            message :'Berhasil Register'})
                    }
                })
            }
        })
    }

    static findAll(req,res){
        fs.readFile(path,(err,data)=>{
            if(err){
                res.status(400).json({
                    error : 'eror baca data'
                })
            }else{
                let realData = JSON.parse(data)
                res.status(200).json({
                    message : 'Berhasil get karyawan',
                    data : realData.users
                })
            }
        })
    }
    static login(req,res){
        fsPromise.readFile(path)
        .then((data) => {
            let employees = JSON.parse(data)
            
            let {users} = employees

            let {name,password} = req.body

            const indexEmp = users.findIndex(user => user.name == name)
            if(indexEmp == -1){
                res.status(404).json({error:'data tidak ditemukan'})
            }else{
                let employee = users[indexEmp]

                if(employee.password == password){
                    employee.isLogin = true

                    users.splice(indexEmp, 1, employee)
                    return fsPromise.writeFile(path, JSON.stringify(employees))
                }else{
                    res.status(404).json({error:'password salah'})
                }
            }
        })
        .then(()=>{
            res.status(200).json({message:'Berhasil login'})
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}

module.exports = employeeController;