import Employee from "./employee";
import Student from './student';
import Trainer from "./trainer";
import fs from 'fs'
import fsPromises from "fs/promises"

const path = "data.json";

class Bootcamp{
    static register(input){
        let [name, password, role] = input.split(',')
        fs.readFile(path,(err, data)=>{
            if(err){
                console.log(err)
            }
            
            let existingData = JSON.parse(data)
            let employee = new Employee(name, password, role)
            existingData.push(employee)
            fs.writeFile(path,JSON.stringify(existingData), (err) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log('Berhasil Register')
                }
            })
        })
    }

    static login(input) {
        let [name, password] = input.split(",")

        fsPromises.readFile(path).then((data) => {
            let employees = JSON.parse(data)
            let indexEmp = employees.findIndex((emp) => emp._name == name)

            if (indexEmp == -1) {
                console.log("Data Tidak Ditemukan");
            } else {
                let employee = employees[indexEmp]

                if (employee._password == password) {
                    employee._isLogin = true

                    employees.splice(indexEmp, 1, employee)
                    return fsPromises.writeFile(path, JSON.stringify(employees)).then(() => {
                        console.log("Berhasil Login");
                    })
                } else {
                    console.log("Password yang Dimasukkan Salah");
                }
            }
        }).catch((err) => {
            console.log(err);
        })
    }

    static async addSiswa(input) {
        const [name, trainerName] = input.split(",")

        await fsPromises.readFile(path).then((data) => {
            let dataJSON = JSON.parse(data)

            let indexAdminLogin = dataJSON.findIndex((trn) => trn._role == "admin")
            if (indexAdminLogin == -1) {
                console.log("Tidak Ada Role Admin");
            } else {
                let admin = dataJSON[indexAdminLogin]
                if (admin._isLogin == false) {
                    console.log('Tidak Ada Admin yang Login');
                } else {
                    let indexTrainer = dataJSON.findIndex((trn) => trn._name == trainerName)
                    let siswa = new Student(name)

                    if (indexTrainer == -1) {
                        let trainer = new Trainer(trainerName)
                        trainer._students.push(siswa)
                        dataJSON.push(trainer)
                    } else {
                        let trainer = dataJSON[indexTrainer]
                        trainer._students.push(siswa)
                        dataJSON.splice(indexTrainer, 1, trainer)
                    }

                    return fsPromises.writeFile(path, JSON.stringify(dataJSON)).then(() => {
                        console.log("Berhasil add siswa");
                    })
                }
            }
        }).catch((err) => {
            console.log(err);
        })
    }
}

export default Bootcamp;