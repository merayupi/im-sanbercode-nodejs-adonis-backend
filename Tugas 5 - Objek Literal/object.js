
// Soal 1
console.log('Soal 1')
function arrayToObject(arr){

    for(var i = 0; i < arr.length; i++){

        var thisYear = (new Date()).getFullYear();
        var personArr = arr[i];

        var objPerson = {
            first_Name: personArr[0],
            last_Name: personArr[1],
            gender: personArr[2]
        }

        if(!personArr[3] || personArr [3] > thisYear){
            objPerson.age = 'Invalid Birth Year'
        } else{
            objPerson.age = thisYear - personArr[3]
        }

        var fullname = objPerson.first_Name + ' ' + objPerson.last_Name
        console.log(`${i+1}. ${fullname} :  `,objPerson)
    }
    
}


var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people)
arrayToObject(people2)
arrayToObject([])//error case


// Soal 2
console.log('Soal 2')
function naikAngkot(Penumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var output = []

        for(var j = 0; j < Penumpang.length; j++){
            var harga = 2000;
            var Penumpangskrg = Penumpang[j];

            var obj = {
                penumpang: Penumpangskrg[0], 
                naikDari: Penumpangskrg[1], 
                tujuan: Penumpangskrg[2]
            }

            var bayar = (rute.indexOf(Penumpangskrg[2]) - rute.indexOf(Penumpangskrg[1])) * harga
            
            obj.bayar = bayar
            output.push(obj)
        }
        return output;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]


// Soal 3
console.log('Soal 3')
function nilaiTertinggi(siswa) {
    var output ={}
    for( i = 0; i < siswa.length; i++){

        if (!output[siswa[i].class]){
            output[siswa[i].class] = {
                "name": siswa[i].name,
                "score":siswa[i].score
            }
        }else {
            if(output[siswa[i].class].score < siswa[i].score){
                output[siswa[i].class] = {
                    "name": siswa[i].name,
                    "score":siswa[i].score
                }
        } 
    } 
  }console.log(output)
}

  console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]));

  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]));