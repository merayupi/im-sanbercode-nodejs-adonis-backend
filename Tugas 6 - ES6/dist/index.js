"use strict";

var _command = require("./lib/command");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var myArgs = process.argv.slice(2);
var command = myArgs[0];

switch (command) {
  case 'sapa':
    var name = myArgs[1];
    console.log((0, _command.sapa)(name));
    break;

  case 'convert':
    var data = myArgs.slice(1);

    var _data = _slicedToArray(data, 3),
        nama = _data[0],
        domisili = _data[1],
        umur = _data[2];

    console.log((0, _command.biodata)(nama, domisili, umur));
    break;

  case 'checkScore':
    var dataScore = myArgs.slice(1) + '';
    var a = dataScore.split(":");

    for (var i = 0; i < 1; i++) {
      var b = a.toString().split(',');

      var _b = _slicedToArray(b, 6),
          key = _b[0],
          x = _b[1],
          kelas = _b[2],
          y = _b[3],
          score = _b[4],
          z = _b[5];

      console.log((0, _command.check)(x, y, z));
    }

    break;

  case 'filterData':
    var datafilter = myArgs[1];
    console.log((0, _command.filterData)(datafilter));
    break;

  default:
    console.log('Sorry, that is not something I know how to do.');
}