const { Venues } = require('../models')
const venues = require('../models/venues')

class VenuesController {
  static async store(req, res) {
    try {
      let name = req.body.name
      let address = req.body.address
      let phone = req.body.phone

      const newVenue = await Venues.create({
        name: name,
        address: address,
        phone: phone,
      })

      res.status(200).json({
        status: 'success',
        message: 'Venues berhasil ditambah',
      })
    } catch (error) {
      res.status(401).json({
        status: 'Failed',
        mesage: 'gagal mennyimpan data',
        msg: error.errors.map((e) => e.message),
      })
    }
  }

  static async index(req, res) {
    const venues = await Venues.findAll()

    res.status(200).json({
      message: 'sukses berhasil',
      data: venues,
    })
  }

  static async show(req, res) {
    try {
      let idVenues = req.params.id

      let dataVenuesDetail = await Venues.findByPk(idVenues)
      res.status(200).json({
        status: 'Sukses',
        data: dataVenuesDetail,
      })
    } catch (error) {
      res.status(401).json({
        status: 'Failed',
        message: 'data gagal ditampilkan id tidak ditemukan',
        msg: error,
      })
    }
  }

  static async update(req, res) {
    try {
        let name = req.body.name
        let address = req.body.address
        let phone = req.body.phone

        await Venues.update({
            name: name,
            address: address,
            phone: phone,
        },
        {
            where:{
                id: req.params.id
            }
        }
      )
      res.status(200).json({
        status: 'success',
        message: 'Berhasil Update data',
      })

    } catch (error) {
      res.status(401).json({
        status: 'Failed',
        mesage: 'gagal update data',
        msg: error.errors.map((e) => e.message),
      })
    }
  }

  static async destroy(req,res){
    try {
        await Venues.destroy({
            where: {
                id: req.params.id

            }
        })

        res.status(200).json({
            status: 'success',
            message: 'Berhasil Delete data',
          })
    } catch (error) {
        res.status(401).json({
            status: 'Failed',
            mesage: 'gagal menghapus data'
          })
    }
  }
}

module.exports = VenuesController
