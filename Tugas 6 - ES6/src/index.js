import {sapa, biodata, check, filterData} from './lib/command'

const myArgs = process.argv.slice(2);
const command = myArgs[0]

switch (command) {
  case 'sapa':
    let name = myArgs[1]
    console.log(sapa(name));
    break;

  case 'convert':
    const data = myArgs.slice(1)
    let [ nama, domisili, umur ] = data
    console.log(biodata(nama,domisili,umur));
    break;
 
 case 'checkScore':
    const dataScore = myArgs.slice(1) + ''
    const a = dataScore.split(":");
        for(var i = 0;i<1;i++){
            var b = a.toString().split(',')
            let [key,x,kelas,y,score,z] = b
            console.log(check(x,y,z))
        }
    break;
 
 case 'filterData':
    let datafilter = myArgs[1]
    console.log(filterData(datafilter))
    break;
  default:
    console.log('Sorry, that is not something I know how to do.');
}