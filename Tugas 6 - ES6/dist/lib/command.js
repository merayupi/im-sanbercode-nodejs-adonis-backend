"use strict";

var sapa = function sapa(nama) {
  return "halo selamat pagi, ".concat(nama);
};

var biodata = function biodata(nama, domisili, umur) {
  return {
    nama: nama,
    domisili: domisili,
    umur: Number(umur)
  };
};

var check = function check(name, kelas, score) {
  return {
    name: name,
    kelas: kelas,
    score: Number(score)
  };
};

var data = [{
  name: "Ahmad",
  kelas: "adonis"
}, {
  name: "Regi",
  kelas: "laravel"
}, {
  name: "Bondra",
  kelas: "adonis"
}, {
  name: "Iqbal",
  kelas: "vuejs"
}, {
  name: "Putri",
  kelas: "Laravel"
}];

var filterData = function filterData(kelas) {
  for (var i = 0; i < data.length; i++) {
    return data.filter(function (el) {
      return el['kelas'].toLowerCase().includes(kelas.toLowerCase());
    });
  }
};

exports.sapa = sapa;
exports.biodata = biodata;
exports.check = check;
exports.filterData = filterData;