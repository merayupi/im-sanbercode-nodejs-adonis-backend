
console.log('Soal 1')
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(n){

    for (i = 0; i < n.length; i++) {

        data = `Nomor ID: ${n[0+i][0]}\n`+
                `Nama Lengkap: ${n[0+i][1]}\n`+
                `TTL: ${n[0+i][2]} ${n[0+i][3]}\n`+
                `Hobi: ${n[0+i][4]}\n`
        console.log(data)
    }
       
}

dataHandling(input)

// Soal 2
console.log('Soal 2')
function reverse(str){
    var output=""
    var panjangstring = str.length

    for(var i = panjangstring -1; i >= 0; i--){
        output+=str[i]
    }
    return output;
}

console.log(reverse("SanberCode")) 
console.log(reverse("racecar")) 
console.log(reverse("kasur rusak"))
console.log(reverse("haji ijah"))
console.log(reverse("I am Sanbers"))

