
class Kelas{
    constructor(name, level, instructor){
        this._name = name
        this._students = []
        this._level = level
        this._instructor = instructor
    }

    get name(){
        return this._name
    }

    set name(strname){
        this._name = strname
    }

    get level(){
        return this._level
    }

    set level(strlevel){
        this._level = strlevel
    }

    get instructor(){
        return this._instructor
    }

    set instructor(strinstructor){
        this._instructor = strinstructor
    }

    addStudent(student){
        this._students.push(student)
    }
}

export default Kelas;