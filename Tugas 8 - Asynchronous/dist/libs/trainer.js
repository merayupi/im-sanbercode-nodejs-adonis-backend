"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Trainer = /*#__PURE__*/function () {
  function Trainer(name) {
    _classCallCheck(this, Trainer);

    this._name = name;
    this._password = "123456";
    this._role = "trainer";
    this._isLogin = false;
    this._students = [];
  }

  _createClass(Trainer, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(value) {
      this._name = value;
    }
  }, {
    key: "password",
    get: function get() {
      return this._password;
    },
    set: function set(value) {
      this._password = value;
    }
  }, {
    key: "role",
    get: function get() {
      return this._role;
    },
    set: function set(value) {
      this._role = value;
    }
  }, {
    key: "isLogin",
    get: function get() {
      return this._isLogin;
    },
    set: function set(value) {
      this._isLogin = value;
    }
  }, {
    key: "addStudent",
    value: function addStudent(student) {
      this._students.push(student);
    }
  }]);

  return Trainer;
}();

var _default = Trainer;
exports["default"] = _default;